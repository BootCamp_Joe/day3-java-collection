package com.thoughtworks.collection;

import java.util.List;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {
        return numbers.stream()
                .reduce(0, (lastOdd, number) -> number % 2 == 1? number : lastOdd);

    }

    public String getLongest(List<String> words) {
        return words.stream()
                .reduce("", (longest, word) -> word.length() > longest.length()? word: longest);
    }

    public int getTotalLength(List<String> words) {
        return words.stream()
                .reduce("", (len, word) -> len + word)
                .length()
                ;
    }
}
